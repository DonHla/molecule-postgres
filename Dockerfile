FROM ubuntu:18.04

RUN yum -qy update
RUN yum install -qy python software-properties-common git
RUN yum-add-repository --yes --update ppa:ansible/ansible
RUN yum install -qy ansible ansible-lint

CMD ["/bin/bash"]