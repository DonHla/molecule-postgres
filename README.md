# Базовая установка и конфигурация PostgreSQL

Данная роль устанавливает PostgreSQL версий: 10,11,12.

**В данный момент УАБД поддерживает версии PostgreSQL до 11 версии включительно.**

Установка возможна из локального каталога roles/postgresql/files (пакеты для установки версий 10,11,12 уже присутствуют) или из репозитория.

## Использование роли

Для базовой установки достаточно указать версию PostgreSQL, для этого в корне есть файл postgresql.yml, где в переменной pgdb можно задать следующие значения соответствующие версии СУБД: v10, v11, v12.

Пример файла postgresql.yml с указанной переменной pgdb для установки PostgreSQL версии 11:

```yaml
---
- hosts: all
  gather_facts: false

  roles:
    - role: postgresql
      pgdb: "{{ postgres_version | default('v11') }}"

```

Также в корне есть inventory-fail.yml, можно отредактировать его или использовать свой инвентори файл.

Пример:

```yaml
 all:
  hosts:
    remote:
      ansible_host: remote_host
      remote_user: remote_ivan
    local:
      ansible_host: 127.0.0.1
      ansible_user: root
```

Запускается плэйбук так: ansible-playbook postgresql.yml -i <путь к инвентори .
Если необходимо указать ключи и дополнительные переменные, то так: ansible-playbook postgresql.yml -i <путь к инвентори> -e <дополнительные переменные> --private-key <путь/файл с приватным ssh ключом?

Пример:

```shell
 ansible-playbook postgresql.yml -i inventory-file.yml
```

### Дополнительная конфигурация (опционально)

Если нужно дополнительно создать базы данных, создать пользователей и выдать им права, создать схему для базы данных или изменить доступы в pg_hba.conf, то необходимо в файле roles/postgresql/defaults/main.yml прописать нужные переменные:

Пример переменных для создания нескольких бд:

```yaml
    db:
      db-1:
        db_name: db-1
        owner_name:
        db_template: template0
        encod: UTF-8
        locate: ru_RU.UTF-8
      db-2:
        db_name: db-2
        owner_name:
        db_template: template0
        encod: UTF-8
        locate: ru_RU.UTF-8
```

Пример переменных для создания схемы для бд:

```yaml
    schemas:
      schema-1:
        schema_name: public
        schema_db_name: default
        schema_owner: "{{ login_postgres }}"
```

Пример переменных для создания нескольких пользователей:

```yaml
    users:
      user-1:
        role_name: test-user-1
        role_pw: SuperStrongPassWord20
        role_attributes: SUPERUSER
        data_base: db-1
        priv_db: ALL
        db_schema_name: db-1-schema
        priv_tb: ALL
      user-2:
        role_name: test-user-2
        role_pw: SuperStrongPassWord21
        role_attributes: NOCREATEDB,NOCREATEROLE,NOSUPERUSER
        data_base: db-1
        priv_db:
        db_schema_name:
        priv_tb: SELECT
```

Пример переменных для добавления строк в pg_hba.conf:

```yaml
    conf_strings:
      string-1:
        type_of_rule: host
        target_db: all
        user_pg_hba: all
        address: 0.0.0.0/0
        auth_method: md5
      string-2:
        type_of_rule: host
        target_db: all
        user_pg_hba: all
        address: 127.0.0.2/32
        auth_method: ident
```

*По умолчанию в файле defaults/main.yml прописаны перменные для создания пароля пользователя postgres:*

Пример:

```yaml
    login_postgres: postgres
    postgres_pw: postgres
```

*И переменные для создания дефолтной бд.*

Сами дополнительные задачи находятся в roles/postgresql/tasks/enable-service.yml.
Пример:

```yaml
 ...
- name: "Config bd create create bd"
   include_tasks: config_db/create-some-db.yml
   when: db is defined
 ...
```

Можно также добавить собственные задачи на примере существующих и затем занести их в roles/postgresql/tasks/enable-service.yml.

### Установка из репозитория (опционально)

Установить пакеты из репозитория возможно при указании переменных r_fqdn и r_ip после переменной pgdb в postgresql.yml.

```yaml
 ...
pgdb: v11
r_fqdn: "http://lr24-101lv.vtb24.ru/"
r_ip: "http://10.64.36.176/"
 ...
```

Пути до пакетов и GPG-ключа задаются в roles/postgresql/add-repo-pckg-instl.yml

```yaml
...
baseurl: "{{ r_fqdn }}other/postgres{{ pgdb | replace('v','') }}_el7/ {{ r_ip }}other/postgres{{ pgdb | replace('v','') }}_el7/"
gpgkey: "{{ r_fqdn }}other/postgres{{ pgdb | replace('v','') }}_el7/RPM-GPG-KEY-PGDG"
...
```
